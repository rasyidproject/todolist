const imageKit = require('imagekit')
const config = require('../config/imagekitconfig')
module.exports = new imageKit(config)