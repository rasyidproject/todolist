const { profile } = require('../models')
const imagekit = require('../lib/imagekit')

module.exports = {
    register: async (req, res, next) => {
        try {
            const data = await profile.create({
                id_user: res.data.id
            })
            res.data.profile = data
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    test: async (req, res, next) => {
        console.log(req.file)
        res.data = 'nothing happened'
        next()
    },
    uploadPhoto : async (req , res, next) => {
        // Get current user id from token
        let getId = req.user.id
        
        let getUrl // Define variable to store url information

        if (process.env.NODE_ENV == 'development') {
            console.log('upload photo development mode')

            // Get image url from middleware upload
            getUrl = req.file.url

        } else if(process.env.NODE_ENV == 'production'){
            console.log('upload photo production mode')

            let split = req.file.originalname.split('.')
            let ext = split[split.length-1]
            let name = `user_${getId}_profile.${ext}`

            try {
                const file = await imagekit.upload({
                    file: req.file.buffer, fileName: name
                })

                // Get image url from CDN server
                getUrl = file.url
            } catch (error) {
                next(error)
            }
        } else if(process.env.NODE_ENV == 'test') {
            console.log('upload photo test mode')
        }
        
        try {
            await profile.update({
                image_url: getUrl
            },{
                where: {
                    id_user: getId
                }
            })
            res.status(200)
            res.data = {
                message: `Photo from user id ${getId} is successfully uploaded`,
                url: getUrl
            }
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    getAllProfile: async (req, res, next) => {
        try {
            res.data = await profile.findAll()
            res.status(200)
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    getCurrentProfile: async (req, res, next) => {
        try {
            res.data = await profile.findOne({
                where: {
                    id_user: req.user.id
                }
            })
            res.status(200)
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    }
}