const multer = require('multer')
const path = require('path')

module.exports = {
    development: {
        storage : multer.diskStorage({
            destination: function (req, file, cb) {
              cb(
                  null,
                  path.resolve(__dirname, '..', 'public', 'upload')
              )
            },
            filename: function (req, file, cb) {
              const split = file.originalname.split('.')
              const ext = split[split.length - 1]
              const name = `user_${req.user.id}-` + file.fieldname.toLowerCase() 
                + '-' + Date.now() + `.${ext}`
              file.url = process.env.BASE_URL + '/upload/' + name
              cb(null, name)
            }
        })
    },
    test: {
        storage : multer.diskStorage({
            destination: function (req, file, cb) {
              cb(
                  null,
                  path.resolve(__dirname, '..', 'public', 'upload')
              )
            },
            filename: function (req, file, cb) {

              const split = file.originalname.split('.')
              const ext = split[split.length - 1]
              const name = 'test-' + file.fieldname.toLowerCase() + '-' + Date.now() + `.${ext}`
              file.url = process.env.BASE_URL + '/upload/' + name
              cb(null, name)
            }
        })
    },
    production: {
        limits: {
            fileSize: 2000000
        }
    }
}