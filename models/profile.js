'use strict';
module.exports = (sequelize, DataTypes) => {
  const profile = sequelize.define('profile', {
    image_url: DataTypes.STRING,
    id_user: DataTypes.INTEGER
  }, {});
  profile.associate = function(models) {
    profile.belongsTo(models.user, {
      foreignKey: `id`,
      onDelete: 'CASCADE'
    })
  };
  return profile;
};