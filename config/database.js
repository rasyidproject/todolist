module.exports = {
    development: {
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: `${process.env.DB_NAME}_development`,
        host: "127.0.0.1",
        dialect: "postgres",
        dialectOptions: {
            useUTC: false
        },
        timezone: '+07:00'
    },
    test: {
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: `${process.env.DB_NAME}_test`,
        host: process.env.DB_HOST || "127.0.0.1",
        dialect: "postgres",
        dialectOptions: {
            useUTC: false
        },
        timezone: '+07:00'
    },
    production: {
        use_env_variable: 'DATABASE_URL',
        dialectOptions: {
            useUTC: false
        },
        timezone: '+07:00'
    }
}