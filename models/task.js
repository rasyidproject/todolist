'use strict';
module.exports = (sequelize, DataTypes) => {
  const task = sequelize.define('task', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    deadline: DataTypes.DATE,
    importance: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    complete: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    id_user: {
      type: DataTypes.INTEGER,
      unique: true
    },
    
  }, {});
  task.associate = function(models) {
    task.belongsTo(models.user, {
      foreignKey: `id`,
      onDelete: 'CASCADE'
    })
  };
  return task;
};