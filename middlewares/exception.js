module.exports = [

  // 404 Handler
  function(req, res) {
    res.status(404).json({
      status: 'fail',
      errors: ["404 not found!"]
    });
  },

  // Exception Handler
  function(err, req, res, next) {
    res.status(res.statusCode == 200 ? 500 : res.statusCode).json({
      status: 'fail',
      errors: [err.message]
    });
  }

]