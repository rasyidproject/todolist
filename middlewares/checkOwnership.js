const models = require('../models');

module.exports = (modelName) => {
  const Model = models[modelName];

  return async function(req, res, next) {
    try {
      let instance = await Model.findByPk(req.params.id)

      if (instance.id_user !== req.user.id) {
        res.status(403);
        return next(new Error(`Dont cheat !! This is not your ${modelName}`))
      }
      next();
    } catch (error) {
      next(error)
    }
  }
}