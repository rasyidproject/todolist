const { user } = require('../models')
const bcrypt = require('bcryptjs')

module.exports = {
    register: async (req, res, next) => {
        try {
            const data = await user.create({
                name: req.body.name,
                email: req.body.email,
                encrypted_password: req.body.password
            })
            res.data = data.entity
            res.status(201)
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    login: async (req, res, next) => {
        try {
            const data = await user.authenticate(req.body)
            res.status(200)
            res.data = data.entity
            next()
        } catch (error) {
            res.status(401)
            next(error)
        }
    },
    getCurrentUser: async (req, res, next) => {
        try {
            res.status(200)
            // Show Current User
            res.data = await user.findOne({
                where: {id: req.user.id}
            })
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    update: async (req, res, next) => {
        try {
            //Get current user id from token
            let getId = req.user.id

            // If password is exist in body, hash first before updating to database
            if(req.body.password != null)
                req.body.encrypted_password = bcrypt.hashSync((req.body.password).toString(),10)

            // Update data
            await user.update(req.body,{
                where: {
                    id:getId
                }
            })

            // Show data after update
            const data = await user.findOne({
                where: {
                    id: getId
                }
            })
            res.status(200)
            res.data = {
                message:`data from user id ${getId} is successfully updated`,
                data
            }
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    delete: async (req, res, next) => {
        try {
            //Get current user id from token
            let getId = req.user.id

            // Delete data
            await user.destroy({
                where: {
                    id:getId
                }
            })
            res.status(200)
            res.data = `data from id ${getId} is deleted`
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    all: async (req, res, next) => {
        try {
            // Show All Users
            res.status(200)
            res.data = await user.findAll()
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    allRender: async (req, res, next) => {
        try {
            // Show All Users
            res.status(200)
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    sendVerification: async (req, res, next) => {
        const sgMail = require('@sendgrid/mail');
        sgMail.setApiKey(process.env.SENDGRID_API_KEY);
        try {
            console.log(req.user.email)
            const msg = {
                to: 'rasyidproject@gmail.com',
                from: 'rasyidproject@gmail.com',
                subject: 'Sending with Twilio SendGrid is Fun',
                text: 'and easy to do anywhere, even with Node.js',
                html: '<strong>and easy to do anywhere, even with Node.js</strong>',
            };
            await sgMail.send(msg)
            next()
        } catch (error) {
            console.log(error)
            next(error)

            if (error.response) {
                console.error(error.response.body)
            }
        }
    }
}