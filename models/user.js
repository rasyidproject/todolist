'use strict';

const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    name: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'Email address already in use!'
    },
      validate: {
        isEmail: true,
        isLowercase: true,
        notEmpty: true
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    verified: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
      validate: {
        notEmpty: true
      }
    }
  }, {
    hooks: {
      beforeValidate: instance => {
        if(instance.email != null )
          instance.email = instance.email.toLowerCase();
      },

      beforeCreate: instance => {
        instance.encrypted_password = bcrypt.hashSync(instance.encrypted_password, 10);
      },
    }
  });
  user.associate = function(models) {
    user.hasOne(models.profile, {
      foreignKey: `id_user`
    })

    user.hasMany(models.task, {
      foreignKey: `id_user`
    })
  };

  Object.defineProperty(user.prototype, 'entity', {
    get() {
      return {
        id: this.id,
        email: this.email,
        name: this.name,
        access_token : this.getToken()
      }
    }
  })

  user.authenticate = async function({email,password}){
    try {
      let instance = await this.findOne({
        where : { email: email.toLowerCase() }
      })
      if (instance == null) return Promise.reject(new Error(`Email doesn't exist`))

      let isValidPassword = instance.checkCredential(password)
      if(!isValidPassword) return Promise.reject(new Error("Wrong password"))

      return Promise.resolve(instance)
    } catch (err) {
      return Promise.reject(new Error(err.message))
    }
  }

  user.prototype.checkCredential = function(password) {
    return bcrypt.compareSync(password, this.encrypted_password)
  }

  user.prototype.getToken = function() {
    return jwt.sign({
      id: this.id,
      email: this.email
    }, process.env.TOKEN)
  }

  return user;
};