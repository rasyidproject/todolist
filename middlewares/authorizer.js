const jwt = require('jsonwebtoken');
const {user} = require('../models');

module.exports = async (req, res, next) => {
  try {
    let token = req.headers.authorization;
    let payload = jwt.verify(token, process.env.TOKEN);
    req.user = await user.findByPk(payload.id);
    next(); 
  }

  catch {
    res.status(401);
    next(new Error("Invalid Token"));
  }
}