const express = require('express')
const app = express()
require('dotenv').config()

// Core Module
const exception = require('./middlewares/exception')
const router = require('./router')

app.use(express.json())
app.use(express.static('public'))
app.use(express.urlencoded({ extended: false }))
app.set('view engine', 'pug')

app.get('/', (req, res) => {
    res.render('index')
})

app.use('/api/v1', router)

// Apply Exception Handler
exception.forEach(handler => 
    app.use(handler)
)

module.exports = app