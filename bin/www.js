#!/usr/bin/env_node

const app = require('../app')
const PORT = process.env.PORT || 3000

app.listen(PORT, () => {
    console.log(`Server started at ${Date()}`)
    console.log(`Listening on port ${PORT}`)
    console.log('My Database Url:', process.env.DATABASE_URL)
    console.log(`App run on ${process.env.NODE_ENV} mode`)
})