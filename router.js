const router = require('express').Router()


// Controller
const user = require('./controllers/userController')
const profile = require('./controllers/profilController')
const task = require('./controllers/taskController')

// Middleware
const success = require('./middlewares/success')
const checkToken = require('./middlewares/authorizer')
const checkUser = require('./middlewares/checkOwnership')
const uploader = require('./middlewares/uploader')

// Setup swagger documentation
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')

// API Register
router.post('/register', 
    user.register, 
    profile.register, success)

// API Users Collection
router.post('/users', user.login, success)
router.put('/users', checkToken, user.update, success)
router.delete('/users', checkToken, user.delete, success)
router.get('/users', checkToken, user.getCurrentUser, success)
router.get('/users/all',user.all, success)
router.get('/users/verify', checkToken, user.sendVerification, success)

// API Task Collection
router.post('/tasks', checkToken, task.add, success)
router.put('/tasks/:id', checkToken, checkUser('task'),
    task.update, success)
router.delete('/tasks/:id', checkToken, checkUser('task'), 
    task.delete, success)
router.get('/tasks', checkToken, task.list, success)
router.get('/tasks/order/:orderBy', checkToken, task.listOrder, success)

// API Profile
router.put('/profile', checkToken, uploader.single('image'), profile.uploadPhoto, success)
router.get('/profile', checkToken, profile.getCurrentProfile, success)
router.get('/profile/all', profile.getAllProfile, success)

// Swagger implementation
router.use('/documentation', swaggerUi.serve)
router.get('/documentation', swaggerUi.setup(swaggerDocument))

//Admin
// router.get('/login', )

module.exports = router