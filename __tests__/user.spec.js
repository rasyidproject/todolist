const app = require('../app')
const request = require('supertest')
const { user, task } = require('../models')

let getToken
let user_barry = {
    name: 'Barry Allen',
    email: 'barry@starlab.com',
    password: '12345'
}
let user_cisco = {
    name: 'Cisco Ramon',
    email: 'cisco@starlab.com',
    password: '12345'
}
describe ('User API Collection', () => {

    beforeAll( () => {
        return user.destroy({
            truncate: true,
            cascade: true,
            restartIdentity: true
        })
    })

    newUser = (user) => {
        describe('POST /register', () => {
            test('Should succesfully create a new users', done => {
                try {
                    request(app).post('/api/v1/register')
                    .set('Content-Type','application/json')
                    .send({
                        name: user.name, 
                        email: user.email,
                        password: user.password
                    })
                    .then(res => {
                        expect(res.statusCode).toEqual(201)
                        expect(res.body.status).toEqual('success')
                        expect(res.body.data).toHaveProperty('access_token')
                        done()
                    })
                } catch (error) {
                    console.log(error.message)
                }
            })
        })
    }

    newUser(user_barry)
    newUser(user_cisco)

    login = (user) => {
        describe('POST /users', () => {
            test('Should succesfully login', done => {
                try {
                    request(app).post('/api/v1/users')
                    .set('Content-Type','application/json')
                    .send({
                        name: user.name, 
                        email: user.email,
                        password: user.password
                    })
                    .then(res => {
                        console.log(res.body)
                        expect(res.statusCode).toEqual(200)
                        expect(res.body.status).toEqual('success')
                        expect(res.body.data).toHaveProperty('access_token')
                        getToken = res.body.data.access_token
                        done()
                    })
                } catch (error) {
                    console.log(error.message)
                }
            })
        })
    }

    login(user_barry)

    describe('PUT /users', () => {
        test('Should succesfully update data', done => {
            try {
                request(app).put('/api/v1/users')
                .set('Content-Type','application/json')
                .set('Authorization',getToken)
                .send({
                    name: 'The Flash', 
                    email: 'theflash@starlab.com'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200)
                    expect(res.body.status).toEqual('success')
                    expect(res.body.data.data.name).toEqual('The Flash')
                    expect(res.body.data.data.email).toEqual('theflash@starlab.com')
                    done()
                })
                .catch(e => console.log(e.message))
            } catch (error) {
                console.log(error.message)
            }
        })
    })

    describe('DELETE /users', () => {
        test('Should succesfully delete data', done => {
            try {
                request(app).delete('/api/v1/users')
                .set('Content-Type','application/json')
                .set('Authorization',getToken)
                .then(res => {
                    expect(res.statusCode).toEqual(200)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
                .catch(e => console.log(e.message))
            } catch (error) {
                console.log(error.message)
            }
        })
    })
})

describe('Task API Collection', () => {
    newUser(user_barry)
    login(user_barry)

    describe('POST /tasks', () => {
        test('Should succesfully create new post', done => {
            request(app).post('/api/v1/tasks')
                .set('Content-Type','application/json')
                .set('Authorization',getToken)
                .send({
                    name: 'jogging', 
                    description: 'jog for 3 km at embung tambakboyo',
                    deadline: new Date().getDate() + 1, // 1 day from now
                    importance: false
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body.data).toHaveProperty('name')
                    done()
                })
                .catch(e => console.log(e.message))
        })
    })

    describe('PUT /tasks', () => {
        test('Should succesfully update post', done => {
            let id = 1
            request(app).put(`/api/v1/tasks/${id}`)
                .set('Content-Type','application/json')
                .set('Authorization',getToken)
                .send({
                    name: 'running', 
                    description: 'run for 5 km at embung tambakboyo',
                    deadline: new Date().getDate() + 1, // 1 day from now
                    importance: false
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200)
                    expect(res.body.status).toEqual('success')
                    expect(res.body.data).toHaveProperty('data')
                    done()
                })
                .catch(e => console.log(e.message))
        })
    })

    describe('DELETE /tasks', () => {
        test('Should succesfully delete post', done => {
            let id = 1
            request(app).delete(`/api/v1/tasks/${id}`)
                .set('Content-Type','application/json')
                .set('Authorization',getToken)
                .then(res => {
                    expect(res.statusCode).toEqual(200)
                    expect(res.body.status).toEqual('success')
                    expect(res.body.data).toHaveProperty('message')
                    done()
                })
                .catch(e => console.log(e.message))
        })
    })
})