const { user } = require('../models')
const bcrypt = require('bcryptjs')

module.exports = {
    index: (req,res) => {
        res.status(200).json({
            status: 'success',
            message: 'Hello Mars'
        })
    },
    registerNext: async (req, res, next) => {
        try {
            const data = await user.create({
                name: req.body.name,
                email: req.body.email,
                encrypted_password: req.body.password
            })
            res.data = data.entity
            res.status(201)
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    register: async (req, res) => {
        try {
            const data = await user.create({
                name: req.body.name,
                email: req.body.email,
                encrypted_password: req.body.password
            })
            return res.status(201).json({
                status: 'success',
                data: {
                    data
                }
            })
        } catch (error) {
            return res.status(422).json({
                status: 'fail',
                errors: [error.message]
            })
        }
    },
    registerPromise: (req, res) => {
        return user.create(req.body)
            .then(data => {
                return res.status(201).json({
                    status: 'success',
                    data: {
                        data
                    }
                })
            })
            .catch(error => {
                res.status(422).json({
                    status: 'fail',
                    errors: [error.message]
                })
            })
    }
}