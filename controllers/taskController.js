const { task } = require('../models')

module.exports = {
    add: async(req, res, next) => {
        try {
            res.data = await task.create({
                name: req.body.name,
                description: req.body.description,
                deadline: req.body.deadline,
                importance : req.body.importance,
                complete : req.body.complete,
                id_user : req.user.id
            })
            res.status(201)
            next()
        } catch (error) {
            next(error)
        }
    },
    update: async(req, res, next) => {
        try {
            // Get current id post from parameter
            let getId = req.params.id

            await task.update(req.body,{
                where: {
                    id: getId
                }
            })

            // Get data after update
            let data = await task.findOne({
                where: {
                    id: getId
                }
            })

            res.data = {
                message: `data from task id ${getId} is updated`,
                data
            }
            res.status(200)
            next()
        } catch (error) {
            next(error)
        }
    },
    delete: async(req, res, next) => {
        try {
            let getId = req.params.id
            await task.destroy({
                where: {
                    id: getId
                }
            })

            res.data = {
                message: `data from task id ${getId} is deleted`
            }
            res.status(200)
            next()
        } catch (error) {
            next(error)
        }
    },
    list: async(req, res, next) => {
        try {
            res.data = await task.findAll({
                limit: 10,
                where: {
                    id_user: req.user.id
                }
            })
            res.status(200)
            next()
        } catch (error) {
            next(error)
        }
    },
    listOrder: async(req, res, next) => {
        // Get order choice from parameter
        let orderBy = req.params.orderBy
        
        try {
            res.data = await task.findAll({
                limit: 10,
                where: {
                    id_user: req.user.id
                },
                order: [
                    [orderBy, 'DESC']
                ]
            })
            res.status(200)
            next()
        } catch (error) {
            next(error)
        }
    }
}